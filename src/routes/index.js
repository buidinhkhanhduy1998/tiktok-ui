import { Fragment } from 'react';
import Home from '@/pages/Home';
import Upload from '@/pages/Upload';
import Search from '@/pages/Search';
import Profile from '@/pages/Profile';
import Following from '@/pages/Following';
import { HeaderOnly } from '@/components/Layout';

const publicRoutes = [
  { path: '/', component: Home },
  { path: '/following', component: Following },
  { path: '/profile', component: Profile },
  { path: '/upload', component: Upload, layout: HeaderOnly },
  { path: '/search', component: Search, layout: Fragment },
];

const privateRoutes = [];

export { publicRoutes, privateRoutes };
